FROM continuumio/miniconda3

RUN conda install -y numpy \
      && conda install -y matplotlib \
      && conda install -y pytorch cuda80 -c soumith \
      && conda install -y torchvision \
      && conda install -y tqdm \
      && conda install -y scikit-learn \
      && conda install -y jupyter

EXPOSE 9999

WORKDIR /home
