# What

Image for Python 3.6 with:

- pytorch (GPU-ready)
- numpy
- matplotlib
- jupyter

# How

## Building:

```
docker build -f Dockerfile . -t pytorch
```

## Running:

```
docker run -dit --name pytorch --mount type=bind,src="<source_directory>",target="/home" -p 9999:9999 pytorch bash
```

## Exec into it

```
docker exec -it pytorch bash
```

## Run Jupyter notebook:

```
(base) root@<container_id>:/# jupyter notebook --ip 0.0.0.0 --port 9999 --allow-root
```
